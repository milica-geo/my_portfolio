'use strict'

let tabela = document.getElementById('tabelaApi');
let ispisPodataka;



async function ispisUTabeli(){
    let url = await fetch ('https://api.spacexdata.com/v4/history');
    let podaci= await url.json();
    ispisPodataka = podaci;
    console.log(ispisPodataka);
    ispisiPodatke();
  
}

function ispisiPodatke(){
    
    for (let i = 0; i < ispisPodataka.length; i++) {
        tabela.innerHTML += `
        <tr>
        <td><a href="${ispisPodataka[i].links.article}">${ispisPodataka[i].links.article}</a></td>
        <td>${ispisPodataka[i].id}</td>
        <td>${ispisPodataka[i].title}</td>
        <td>${ispisPodataka[i].details}</td>
        <td>${ispisPodataka[i].event_date_utc}</td>
        <td>${ispisPodataka[i].event_date_unix}</td>
        
        
        </tr>
        `
    }

};

document.getElementById('btn').addEventListener('click', ispisUTabeli);
