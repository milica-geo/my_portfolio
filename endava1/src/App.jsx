//import logo from './logo.svg';
import "./App.css";
import { Routes, Route} from "react-router-dom";
import Home from "./Home/Home";
import Appointments from "./Pages/Appointments";
import Clients from "./Pages/Clients";
import ViewClients from "./components/ViewClients";
import EditClients from "./components/EditClient";
import AddClient from './components/AddClient'

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo2} alt="logo" className=".App-logo" />
      </header> */}
      <Routes>
        <Route  path="/" element={<Home />} />
        <Route  path="/clients" element={<Clients />} />
        <Route  path="/appointments" element={<Appointments />} />
        <Route  path="/clients/viewClient/:id" element={<ViewClients />} />
        <Route  path="/clients/editClient/:id" element={<EditClients />} />
        <Route  path="/clients/saveClient" element={<AddClient />} />
      </Routes>
    </div>
  );
}

export default App;
