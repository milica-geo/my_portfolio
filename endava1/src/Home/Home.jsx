import React from "react";
import { Button, ButtonGroup } from "@mui/material";
import { useNavigate } from "react-router-dom";
import logo from "../logo2.jpg";
import '../Style/Btn.css'

function Home() {
  const navigate = useNavigate();
  const navigateToClients = () => navigate("/clients");
  
  const navigateToAppointments = () => {
    navigate("/appointments");
  };

  return (
    <div>
      <div>
        <header className="App-header">
          <img src={logo} alt="logo" />
        </header>
      </div>
      <div>
        <ButtonGroup
          variant="text" 
          aria-label="text button group"
          className="Button-group">
          <Button className='home' onClick={navigateToClients}>
            Clients
          </Button>

          <Button className='home' onClick={navigateToAppointments}>
            Appointments
          </Button>
        </ButtonGroup>
      </div>
    </div>
  );
}

export default Home;
