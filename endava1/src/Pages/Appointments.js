import React from 'react';
import { Button } from '@mui/material';
import { useNavigate } from "react-router-dom";
//import AppointmentsList from '../components/AddAppointment'

const Appointments = () => {
  const navigate = useNavigate();
  const navigateToHome = () => {
    navigate("/");
  };
  return (
    <div>
      <div>
        <h1>Appointments Page</h1>
        <Button onClick={navigateToHome} sx={{
          fontSize: "20px",
          color: "blue",
          "&:hover": { color: "green" },
        }}>Back to Home</Button>
      </div>
      
    </div>)
}
export default Appointments;