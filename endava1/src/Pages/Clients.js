import React from 'react';
import { Button, ButtonGroup } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import ClientTable from '../components/ClientTable'
import '../Style/Btn.css';



const Clients = () => {
  const navigate = useNavigate();
  const navigateToHome = () => {
    navigate("/");
  };
  const navigateToAddClient = () => {
    navigate("/clients/saveClient");
  };

  return (
    <div>
      <div>
        <h1>Clients Page</h1>
        <ButtonGroup
          variant="text"
          aria-label="text button group"
          className="Button-group">
          <Button onClick={navigateToHome} className='backBtn'>Home Page</Button>
          <Button onClick={navigateToAddClient} className='backBtn' >Add Client</Button>
        </ButtonGroup>
      </div>
      <div>
        {/* <AddClient />       */}
        <ClientTable />
      </div>
    </div>)
}
export default Clients;
//importovati ClientTable ovde umesto AddClient