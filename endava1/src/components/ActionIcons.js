import * as React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Tooltip, IconButton } from '@mui/material';
import DeleteDialog from './DeleteDialog';
import { useNavigate } from 'react-router-dom';
import '../Style/Icons.css';
import '../Style/Table.css';

function ActionIcons({ id, fetchData, setToastOpen}) {
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();
  const navigateToViewClient = () => {
    navigate(`/clients/viewClient/${id}`);
  }
  const navigateToEditClient = () => {
    navigate(`/clients/editClient/${id}`);
  }
  
  

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const deleteClient = async () => {
    return await fetch(`http://localhost:3001/client?id=${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },

    })
      .then(
        (result) => {
          console.log(result)
          if (result['status'] === 200) {
            fetchData();
            setToastOpen({ open: true, message: "Client deleted.", severity: "success" })
          }
        }
      )
      .catch(() => {
        setToastOpen({ open: true, message: "Failed to delete!", severity: "error" })
        handleClose();
      });
     
  }

  return (

    <>
      <Tooltip title="View">
        <IconButton>
          <VisibilityIcon className='view' onClick={navigateToViewClient} />
        </IconButton>
      </Tooltip>
      <Tooltip title="Edit">
        <IconButton>
          <EditIcon className='edit' onClick={navigateToEditClient} />
        </IconButton>
      </Tooltip>
      <DeleteDialog deleteClient={deleteClient} open={open} dialogOpen={handleClickOpen} dialogClose={handleClose} />
    </>
  );
}

export default ActionIcons;


//onClick={()=>deleteClient(props.id)}