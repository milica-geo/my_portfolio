import React from 'react'
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
//import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import { useNavigate, useParams } from 'react-router-dom';
//import Paper from '@mui/material/Paper';
import { useState} from 'react';
import '../Style/Btn.css';
import '../Style/TextField.css';


export default function AddClients({setToastOpen}) {

    //const { id } = useParams();
    const navigate = useNavigate();
    const navigateToHome = () => {
        navigate("/");
    }
    const navigateToClientsTable = () => {
        navigate("/clients");
    }

    const [addClient, setAddClient] = useState({});
    const {name, surname, address, phoneNumber, email } = addClient;
   
    const handleChange = (event) => {
        const{
            target:{
                name,
                value,
            }
        }=event;
         setAddClient({ ...addClient, [name]: value })
         console.log({ ...addClient, [name]: value })
     }
    
    
        const postClient =async(addClient)=>{
             await fetch('http://localhost:3001/saveClient',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                   body: JSON.stringify(addClient)   
            })
           
        }

        return (
        <>
            <h1> Add Client </h1>
            <form >
                <Grid container spacing={2}>
                    <Grid item xs={12} >
                        <TextField
                            className={'text'}
                            required
                            readOnly={false}
                            id='name'
                            variant='outlined'
                            name='name'
                            label='Name'
                            value={name}
                            onChange={handleChange}
                        
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                          className={'text'}
                            required
                            id='surname'
                            variant='outlined'
                            name='surname'
                            label='Surname'
                            value={surname}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                         className={'text'}
                            required
                            id='address'
                            name='address'
                            variant='outlined'
                            label='Address'
                            value={address}
                            onChange={handleChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField
                        className={'text'}
                            required
                            name='phoneNumber'
                            id='phoneNumber'
                            variant='outlined'
                            label='Phone Number'
                            value={phoneNumber}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                         className={'text'}
                            required
                            name='email'
                            id='email'
                            variant='outlined'
                            type='email'
                            label='Email'
                            value={email}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button className='backBtn' onClick={()=>postClient(addClient).then(navigateToClientsTable)} >Save</Button><br></br>
                        <Button className='backBtn' onClick={navigateToClientsTable}>Go Back</Button><br></br>
                        <Button className='backBtn' onClick={navigateToHome}>Home Page</Button>
                    </Grid>
                </Grid>
            </form>
        </>
    )
}

