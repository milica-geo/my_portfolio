import React from 'react'
import {Tooltip} from '@mui/material';


export default function CellFullContent({cellValues}) {
  return (
   <Tooltip title={cellValues.value} >
    <p>{cellValues.value.length<=15 ? cellValues.value : cellValues.value.substring(0, 15) + "..." }</p>
  </Tooltip>

  )
}
