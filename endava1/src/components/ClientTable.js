import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid'
import { useEffect, useState } from 'react';
import ActionIcons from './ActionIcons';
import Toast from './Toast';
import CellFullContent from './CellFullContent';


export default function ClientTable() {

  const [data, setData] = useState([])

  const [toastOpen, setToastOpen] = useState({ open: false, message: "", severity: "" });


  const handleCloseToast = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setToastOpen({ open: false, message: "", severity: "" });
  };

  const fetchData = async () => {
    const result = await fetch('http://localhost:3001/clients')
    const jsonResult = await result.json();
    setData(jsonResult);
  }

  useEffect(() => {
    fetchData();
  },
    [])
  const columns = [
    {
      field: 'name', headerName: 'First name', width: 130, renderCell: (cellValues) =>
        <CellFullContent cellValues={cellValues} />
    },
    {
      field: 'surname', headerName: 'Last name', width: 135, renderCell: (cellValues) =>
        <CellFullContent cellValues={cellValues} />
    },
    {
      field: 'address', headerName: 'Address', width: 135, renderCell: (cellValues) =>
        <CellFullContent cellValues={cellValues} />
    },
    { field: 'phoneNumber', headerName: 'Phone Number', width: 130 },
    {
      field: 'email', headerName: 'Email', width: 130, renderCell: (cellValues) =>
        <CellFullContent cellValues={cellValues} />
    },
    {
      field: 'actions', headerName: 'Actions', type: 'action', width: 180, renderCell: (cellValues) =>

        <ActionIcons id={cellValues.id} fetchData={fetchData} setToastOpen={setToastOpen} />
    }
  ]

  return (
    <div>
      <DataGrid
        className={'table footer'}
        rows={data}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
      <Toast toastOpen={toastOpen} handleCloseToast={handleCloseToast} />

    </div>
  );
}

