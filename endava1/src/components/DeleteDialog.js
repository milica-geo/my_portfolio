import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
// import DialogTitle from '@mui/material/DialogTitle';
// import deleteClient from './ActionIcons';
// import ActionIcons from './ActionIcons';
import DeleteIcon from '@mui/icons-material/Delete';
import { Tooltip, IconButton } from '@mui/material';
import '../Style/Icons.css';

export default function DeleteDialog({deleteClient, dialogOpen, dialogClose, open}) {

  

  return (
    <>
      <Tooltip title="Delete">
        <IconButton>
          <DeleteIcon className='delete' onClick={dialogOpen} />
        </IconButton>
      </Tooltip>
      <Dialog
        open={open}
        onClose={dialogClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure to delete this client?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={dialogClose}>Cancel</Button>
          <Button onClick={deleteClient} autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

