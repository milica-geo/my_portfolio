import React from 'react'
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
//import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import { useNavigate, useParams } from 'react-router-dom';
//import Paper from '@mui/material/Paper';
import { useState, useEffect } from 'react';

import '../Style/Btn.css';
import '../Style/TextField.css';


export default function EditClients() {
    
    const { id } = useParams()
    const navigate = useNavigate();
    const navigateToHome = () => {
        navigate("/");
    }
    const navigateToClientsTable = () => {
        navigate("/clients");
    }

    const [editClient, setEditClient] = useState({});
    const {name, surname, address, phoneNumber, email } = editClient;
   
    const handleChange = (event) => {
        const{
            target:{
                name,
                value,
            }
        }=event;
         setEditClient({ ...editClient, [name]: value })
         console.log({ ...editClient, [name]: value })
     }
    
    useEffect(() => {
         fetch('http://localhost:3001/clients', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }
        )
        .then(result => result.json())
            .then(resultView => setEditClient(resultView.find((row) => row.id === id)))
            .catch((error) =>console.log(error))
            
    },[id]
    )
        const putClient =async(id)=>{
             await fetch('http://localhost:3001/editClient',{
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                   body: JSON.stringify(id)   
            })
            
        }

        return (
        <>
            <h1> Edit Client </h1>
            <form >
                <Grid container spacing={2}>
                    <Grid item xs={12} >
                        <TextField
                            className={'text'}
                            required
                            readOnly={false}
                            id='name'
                            variant='outlined'
                            name='name'
                            value={name}
                            onChange={handleChange}
                        
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                          className={'text'}
                            required
                            id='surname'
                            variant='outlined'
                            name='surname'
                            value={surname}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                         className={'text'}
                            required
                            id='address'
                            name='address'
                            variant='outlined'
                            value={address}
                            onChange={handleChange}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField
                        className={'text'}
                            required
                            name='phoneNumber'
                            id='phoneNumber'
                            variant='outlined'
                            value={phoneNumber}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                         className={'text'}
                            required
                            name='email'
                            id='email'
                            variant='outlined'
                            type='email'
                            value={email}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button className='backBtn' onClick={()=>putClient(editClient).then(navigateToClientsTable)}>Save</Button><br></br>
                        <Button className='backBtn' onClick={navigateToClientsTable}>Go Back</Button><br></br>
                        <Button className='backBtn' onClick={navigateToHome}>Home Page</Button>
                    </Grid>
                </Grid>
            </form>
        </>
    )
}

