import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

export default function Toast({ handleCloseToast , toastOpen }) {

  return (
      <Snackbar open={toastOpen} onClose={handleCloseToast} autoHideDuration={1500} >
        <Alert severity={toastOpen.severity}  sx={{ width: '100%' }} >
          {toastOpen.message}
        </Alert>
      </Snackbar>
    
  );
}


