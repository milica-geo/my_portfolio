import React from 'react'
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
//import Container from '@mui/material/Container';
//import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import { useNavigate, useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import '../Style/Btn.css';
import '../Style/TextField.css';


export default function ViewClients() {

    const { id } = useParams();

    const navigate = useNavigate();
    const navigateToClientsTable = () => {
        navigate("/clients");
    }
    
    const [viewClient, setViewClient] = useState({});
    const { name, surname, address, phoneNumber, email } = viewClient;
    console.log(viewClient);

    useEffect(() => {
        fetch('http://localhost:3001/clients', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },

        })
            .then(result => result.json())
            .then(resultView => setViewClient(resultView.find((row) => row.id === id)))
            .catch((error) =>console.error(error))
    }, [id])

    return (
        <>
            <h1> View Client </h1>
                <form>
                    <Grid container spacing={2} >
                        <Grid item xs={12} >
                            <TextField
                                className={'text'}
                                readonly={true}
                                id='name'
                                variant='outlined'
                                name='name'
                                value={name}
                                
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                className={'text'}
                                readonly={true}
                                id='surname'
                                variant='outlined'
                                name='surname'
                                
                                value={surname}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                className={'text'}
                                readonly={true}
                                id="full-width-text-field"
                                name='address'
                                variant='outlined'
                                
                                value={address}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                className={'text'}
                                readonly={true}
                                name='phoneNumber'
                                id='phoneNumber'
                                variant='outlined'
                                value={phoneNumber}
                                
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                className={'text'}
                                readonly={true}
                                name='email'
                                id='email'
                                variant='outlined'
                                
                                value={email}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button className='backBtn' onClick={navigateToClientsTable}>Go Back</Button>
                        </Grid>
                    </Grid>
                </form>
          
        </>
    )
}
